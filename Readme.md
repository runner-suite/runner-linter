# RuNNer Linter
This project is a fork of the fortran-linter package. It includes
custom changes to reflect the styleguide for RuNNer, the Ruhr-University
Neural Network Energy Representation.

This linter works on a line-by-line basis to enforce some rules regarding the format of Fortran files.

The linter does not ship with any grammar and is solely based on regular expressions. This allows
to easily add new rules, but also implies some limitations.

## Installation

There are 2 ways of installing the linter. The recommended one is through pip

	pip install git+https://gitlab.com/runner-suite/runner-linter.git

The other way, especially suitable for developers, is to clone this repository and install it from the local copy:

	git clone https://gitlab.com/runner-suite/runner-linter.git
	cd runner-linter
	pip install -e .

You might want to perform any one of those steps in a virtual environment.

### Integration Into Text Editors

#### vim
runner-linter can be intergrated with NeoVim 0.2.0+ and Vim 8.0+ using the 
[Asynchronous Lint Engine](https://github.com/dense-analysis/ale). This enables on-the-fly
linting of the current buffer with code-side display of warning messages.

The following tutorial gives a general overview on how to install ALE in vim and how to
configure runner-linter locally as a new linter in ALE.

##### ALE installation
There are many different ways to install packages in vim. One of the simplest is using `packload`:

```bash
# packload looks for packages in all dirs matching ~/.vim/pack/*/start/*.
mkdir -p ~/.vim/pack/git-plugins/start/
git clone https://github.com/dense-analysis/ale.git ~/.vim/pack/git-plugins/start/ale
```

Test the availability of ALE by opening vim and typing `:ALEInfo`, which should
display some helpful ALE status information.

##### Configuring runner-linter in ALE
ALE does not come with an out-of-the-box configuration for this linter. Therefore, we have
to register it using ALE's `ale#linter#DEFINE()` API.

* First, create the necessary directories:
    ```bash
    mkdir -p ~/.vim/ale_linters/fortran
    ```

* Next, copy the following code into the above directory in a file named `runnerlinter.vim`.

    ```vimscript
    " Description: RuNNer 2.0 linter for Fortran files.
    "
    " Get the linter with :
    "
    " pip install git+https://gitlab.com/runner-suite/runner-linter.git
    "
    " or at : https://gitlab.com/runner-suite/runner-linter

    " Set path to fortran-linter executable and necessary linter options.
    call ale#Set('fortran_runnerlinter_executable', '/home/aknoll/.pyenv/versions/runner2/bin/fortran-linter')
    call ale#Set('fortran_runnerlinter_options', ' --syntax-only ')

    function! ale_linters#fortran#runnerlinter#GetExecutable(buffer) abort
        " Populate ale variable with linter executable.
        return ale#Var(a:buffer, 'fortran_runnerlinter_executable')
    endfunction

    function! ale_linters#fortran#runnerlinter#GetCommand(buffer) abort
        " Concatenate executable and options.
        return ale#Escape(ale_linters#fortran#runnerlinter#GetExecutable(a:buffer))
        \   . ale#Var(a:buffer, 'fortran_runnerlinter_options')
        \   . ' %t'
    endfunction

    function! ale_linters#fortran#runnerlinter#Handle(buffer, lines) abort
        " Search for linter errors in the current buffer.
        " Regex pattern to match.
        let l:pattern = '\v^(.*):(\d+):(\d+):(warning|error):(.*)$'

        let l:output = []
        let l:curr_file = ''

        for l:match in ale#util#GetMatches(a:lines, l:pattern)

        call add(l:output, {
        \   'filename': l:curr_file,
        \   'lnum': str2nr(l:match[2]),
        \   'col': str2nr(l:match[3]),
        \   'type': 'W',
        \   'text': "runner-linter : " . l:match[5],
        \})
        endfor

        return l:output
    endfunction

    " Define the new linter for Fortran language.
    call ale#linter#Define('fortran', {
    \   'name': 'runnerlinter',
    \   'output_stream': 'both',
    \   'executable': function('ale_linters#fortran#runnerlinter#GetExecutable'),
    \   'command': function('ale_linters#fortran#runnerlinter#GetCommand'),
    \   'callback': 'ale_linters#fortran#runnerlinter#Handle',
    \})
    ```

* In this script, you will have to change the variable
`fortran_runnerlinter_executable` to match to absolute path to runner-linter on
your system. 

* Finally, enable the linter in your `.vimrc` file (for example in `~/.vimrc`):

    ```vimscript
    let g:ale_linters = {
    \   'fortran': ['runnerlinter'],
    \}
    ```

    You can also enable `gcc` by appending it to this list so you get on-the-fly
    compile messages. 

#### vscode
runner-linter can be integrated into vscode by overloading the `CTRL+S` save operation
with the multicommand package.

* Install the multicommand package with the Extension manager.
* Copy the following three files to your local or project-wide configuration files (files might need to be generated):

keybindings.json
```
[
    {
        "key": "ctrl+s",
        "command": "multiCommand.lintingFortran",
        "when": "resourceExtname == .f90"
    }
]
```

settings.json
```
{
    "multiCommand.commands": [
        {
            "command": "multiCommand.lintingFortran",
            "interval": 500,
            "sequence": [
                "workbench.action.files.save",
                {
                    "command": "workbench.action.tasks.runTask",
                    "args": "Lint Fortran"
                }
            ]
        }
    ]
}
```

tasks.json
```
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "label": "Lint Fortran",
            "type": "shell",
            "command": "/home/aknoll/.pyenv/versions/runner2/bin/fortran-linter --syntax-only ${fileBasename}|| echo 0",
            "options": { "cwd": "${fileDirname}" },
            "presentation": { "reveal": "never" },
            "problemMatcher": {
              "owner": "Fortran",
              "fileLocation": ["relative", "${fileDirname}"],
              "pattern": {
                    "regexp": "^(.*):(\\d+):(\\d+):(warning|error):(.*)$",
                    "file": 1,
                    "line": 2,
                    "column": 3,
                    "severity": 4,
                    "message": 5
              }
            }
          }
    ]
}
```

This will run the linter whenever you use Ctrl+s to save a Fortran file.

## Usage

This tool checks for fortran syntax against a few rules. To print a list of all the warnings for a file, run:

    fortran-linter myfile.f90 --syntax-only --linelength 120

To autofix (most) warnings in place, do:

    fortran-linter myfile.f90 -i

The original file will be backup'ed into `myfile.f90.orig`. All the safe fixes will be done and stored in the file `myfile.f90`.

For more help, you can type

	fortran-linter -h

## Rules

Here is a non-comprehensive set of rules that are enforced.
  * Punctuation should be followed by a space, this include `,`, `;` and `)`.
  * Binary operators (`==`, `+`, ...) should be surrounded by spaces
  * The following special characters are surrounded by at least one space: `::`, `=`.
  * A line should not exceed 120 characters (this is somehow already extreme). The maximum line length can be controlled from the CLI.
  * One should use `use mpi` instead of `include "mpif.h"`. Note that this is not fixed by default as it may break codes where `include "mpif.h"` follows and `implicit none` statement.
  * Spaces are preferred over tabs, trailing whitespaces are cleaned.
  * Warnings are raised if you use `real(8) :: foo`. One should rather use `integer, parameter :: dp = selected_real_kind(15); real(dp) :: foo` or `use iso_fortran_env; real(real64) :: foo`
  * `print` statements should look like `print *, "something"`
  * `write` statements should look like `write(*, *) "something"`
  * Lines should be indented consistently (by default, using an indentation of 4 spaces).
  * No two blank lines behind each other.
  * Double precision real variables should be defined `real(dp)`.
  * Comments should start with a capital letter and end in a dot.
  * Only modules which are actually used should be imported.
